import com.mtim.service.Service;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ServiceTest {

    @Test
    public void testAdd()
    {
        int i = 1;
        String msg = Service.getInstance().getMsg(i);
        assertEquals( i+" Hello World 111", msg);
    }
}
